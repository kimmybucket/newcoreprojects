﻿using System;
using System.Collections.Generic;

namespace ewdb.dblib
{
    public partial class TblUserHost
    {
        public TblUserHost()
        {
            TblTokenRefresh = new HashSet<TblTokenRefresh>();
            TblUserMem = new HashSet<TblUserMem>();
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public string Secondpassword { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public virtual ICollection<TblTokenRefresh> TblTokenRefresh { get; set; }
        public virtual ICollection<TblUserMem> TblUserMem { get; set; }
    }
}
