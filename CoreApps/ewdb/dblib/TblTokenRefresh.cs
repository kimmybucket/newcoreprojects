﻿using System;
using System.Collections.Generic;

namespace ewdb.dblib
{
    public partial class TblTokenRefresh
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public string Jwtid { get; set; }
        public DateTime Creationdate { get; set; }
        public DateTime Expirydate { get; set; }
        public sbyte Used { get; set; }
        public sbyte Invalidated { get; set; }
        public int Userid { get; set; }

        public virtual TblUserHost User { get; set; }
    }
}
