﻿using System;
using System.Collections.Generic;

namespace ewdb.dblib
{
    public partial class TblMenuCategory
    {
        public TblMenuCategory()
        {
            TblMenu = new HashSet<TblMenu>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public int Type { get; set; }

        public virtual TblMenuType TypeNavigation { get; set; }
        public virtual ICollection<TblMenu> TblMenu { get; set; }
    }
}
