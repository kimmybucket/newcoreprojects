﻿using System;
using System.Collections.Generic;

namespace ewdb.dblib
{
    public partial class TblMenuType
    {
        public TblMenuType()
        {
            TblMenuCategory = new HashSet<TblMenuCategory>();
        }

        public int Id { get; set; }
        public string CategoryType { get; set; }
        public string Status { get; set; }

        public virtual ICollection<TblMenuCategory> TblMenuCategory { get; set; }
    }
}
