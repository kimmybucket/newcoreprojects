﻿using System;
using System.Collections.Generic;

namespace ewdb.dblib
{
    public partial class TblUnits
    {
        public TblUnits()
        {
            TblMenu = new HashSet<TblMenu>();
        }

        public int Id { get; set; }
        public string UnitName { get; set; }

        public virtual ICollection<TblMenu> TblMenu { get; set; }
    }
}
