﻿using System;
using System.Collections.Generic;

namespace ewdb.dblib
{
    public partial class TblMenu
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Category { get; set; }
        public string Description { get; set; }
        public double? Volume { get; set; }
        public double Price { get; set; }
        public double? Discount { get; set; }
        public double Totalprice { get; set; }
        public int Unit { get; set; }
        public string Status { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }

        public virtual TblMenuCategory CategoryNavigation { get; set; }
        public virtual TblUnits UnitNavigation { get; set; }
    }
}
