-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: ewalletapp
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_menu`
--

DROP TABLE IF EXISTS `tbl_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_menu` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `category` int(11) NOT NULL,
  `description` varchar(999) DEFAULT NULL,
  `volume` double DEFAULT NULL,
  `price` double NOT NULL,
  `discount` double DEFAULT NULL,
  `totalprice` double NOT NULL,
  `unit` int(11) NOT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'S',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL DEFAULT '1',
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `id_idx` (`category`),
  KEY `unitid_idx` (`unit`),
  CONSTRAINT `categoryid` FOREIGN KEY (`category`) REFERENCES `tbl_menu_category` (`id`) ON UPDATE RESTRICT,
  CONSTRAINT `unitId` FOREIGN KEY (`unit`) REFERENCES `tbl_units` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_menu`
--

LOCK TABLES `tbl_menu` WRITE;
/*!40000 ALTER TABLE `tbl_menu` DISABLE KEYS */;
INSERT INTO `tbl_menu` VALUES (1,'Beer',1,'Let\'s get tipsy!',320,7.9,0,7.9,4,'A','2019-11-14 11:51:38','2019-11-14 12:05:47',1,NULL),(2,'Mahjong',3,'Ji Mo 13-yiu!',0,200,0,200,2,'A','2019-11-14 11:54:10','2019-11-14 12:05:47',1,NULL);
/*!40000 ALTER TABLE `tbl_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_menu_category`
--

DROP TABLE IF EXISTS `tbl_menu_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_menu_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'S',
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_type_id_idx` (`type`),
  CONSTRAINT `categorytypeId` FOREIGN KEY (`type`) REFERENCES `tbl_menu_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_menu_category`
--

LOCK TABLES `tbl_menu_category` WRITE;
/*!40000 ALTER TABLE `tbl_menu_category` DISABLE KEYS */;
INSERT INTO `tbl_menu_category` VALUES (1,'Alcohol','A',2),(2,'Main course','A',2),(3,'Table games','A',3),(4,'Card Games','A',3);
/*!40000 ALTER TABLE `tbl_menu_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_menu_type`
--

DROP TABLE IF EXISTS `tbl_menu_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_menu_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_type` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'S',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_menu_type`
--

LOCK TABLES `tbl_menu_type` WRITE;
/*!40000 ALTER TABLE `tbl_menu_type` DISABLE KEYS */;
INSERT INTO `tbl_menu_type` VALUES (1,'None','A'),(2,'Edibles','A'),(3,'Games','A');
/*!40000 ALTER TABLE `tbl_menu_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_token_refresh`
--

DROP TABLE IF EXISTS `tbl_token_refresh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_token_refresh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(2000) NOT NULL,
  `jwtid` varchar(2000) NOT NULL,
  `creationdate` datetime NOT NULL,
  `expirydate` datetime NOT NULL,
  `used` tinyint(4) NOT NULL DEFAULT '0',
  `invalidated` tinyint(4) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userid_idx` (`userid`),
  CONSTRAINT `userid` FOREIGN KEY (`userid`) REFERENCES `tbl_user_host` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_token_refresh`
--

LOCK TABLES `tbl_token_refresh` WRITE;
/*!40000 ALTER TABLE `tbl_token_refresh` DISABLE KEYS */;
INSERT INTO `tbl_token_refresh` VALUES (1,'d8a70b60-999b-4b8d-8d30-e2b3b9fc303b','abf98684-dfd4-405b-8d2f-6304ecb608e8','2019-11-20 10:41:10','2020-11-20 10:41:10',0,0,1),(2,'14e6e7f9-a982-4583-af6f-b9300afb8498','a75ca9d1-e821-4925-a13c-11ce4859bd65','2019-11-20 10:43:03','2020-11-20 10:43:03',0,0,1),(3,'a96e45c7-13a9-46b6-ac93-ae29bd7b60e7','804b4611-7fc4-4c4f-b4e5-b226e98bc36f','2019-11-20 17:17:19','2020-11-20 17:17:19',0,0,1),(4,'6b93a305-ea4c-4801-8882-bfee38e01a5c','c5c3fcfe-b318-4e79-8b46-7e29d79f20ea','2019-11-20 17:18:38','2020-11-20 17:18:38',0,0,1),(5,'579205f4-335b-44dc-a8db-0bd726582112','b36b93d8-ff98-41d9-b334-572fe9f4de8e','2019-11-20 17:18:47','2020-11-20 17:18:47',0,0,1),(6,'40e6c55d-be92-46a4-870c-5f6677b6e215','6d4a1fe0-c8b6-4dad-801f-3546cc38c51e','2019-11-20 17:19:07','2020-11-20 17:19:07',0,0,1),(7,'16b3bc9f-f757-458e-8752-30d811a3a3de','2451b782-4b4b-4f36-b38a-ff796ad70bc9','2019-11-20 17:20:47','2020-11-20 17:20:47',0,0,1),(8,'984107ac-b3c2-40ba-946c-579696c2f8a1','ee45fd3a-7708-4335-8353-de2e1e0f7edd','2019-11-20 17:21:00','2020-11-20 17:21:00',0,0,1),(9,'a48b92ef-8fa3-4b1f-8585-707aa22e2b6b','631e5a72-d60a-44e5-9dfb-8f44babb7d6d','2019-11-20 17:22:34','2020-11-20 17:22:34',0,0,1),(10,'9a8eb852-c53b-48bb-87b3-e4c568c757e6','5ce846b4-0d15-42a9-9341-572484f4d0b1','2019-11-20 17:24:42','2020-11-20 17:24:42',0,0,1),(11,'287730f6-34bc-4939-abdf-0ccfbb57b8db','81f0d840-1084-4150-baa2-b91809f34f8a','2019-11-20 17:24:53','2020-11-20 17:24:53',0,0,1),(12,'25150bc6-8a76-473f-9605-6fb38f0f36ed','d8ba7e07-c588-4db0-98dc-12cadac33991','2019-11-20 17:27:58','2020-11-20 17:27:58',0,0,1),(13,'6b170105-1d13-467b-8d0f-510b415cd34b','ad041714-c2ea-4c0c-9278-8aa7f418c770','2019-11-20 17:34:49','2020-11-20 17:34:49',0,0,1),(14,'2f2faaec-4f06-47ef-9dd1-468c8b611e53','d4c17feb-1697-42c4-b360-502d334db5d7','2019-11-20 17:36:26','2020-11-20 17:36:26',0,0,1),(15,'a1ef0a78-88df-4699-959b-3b1a5990c752','e6fdd6f8-94fd-4a50-86ed-d32bafa1a958','2019-11-20 17:38:11','2020-11-20 17:38:11',0,0,1);
/*!40000 ALTER TABLE `tbl_token_refresh` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_units`
--

DROP TABLE IF EXISTS `tbl_units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_units`
--

LOCK TABLES `tbl_units` WRITE;
/*!40000 ALTER TABLE `tbl_units` DISABLE KEYS */;
INSERT INTO `tbl_units` VALUES (1,'night'),(2,'game'),(3,'bottle'),(4,'can'),(5,'pack');
/*!40000 ALTER TABLE `tbl_units` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user_host`
--

DROP TABLE IF EXISTS `tbl_user_host`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_user_host` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` longtext NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL DEFAULT '1',
  `updated_by` int(11) DEFAULT NULL,
  `secondpassword` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user_host`
--

LOCK TABLES `tbl_user_host` WRITE;
/*!40000 ALTER TABLE `tbl_user_host` DISABLE KEYS */;
INSERT INTO `tbl_user_host` VALUES (1,'rootadmin','k+m6AL9cBIqTqj+i/rgT3Rrxh9zA1nU4MLQdomRnZ3k=','2019-11-15 14:57:14','2019-11-15 14:57:14',1,NULL,'123456','0164567895','rootadmin@email.com');
/*!40000 ALTER TABLE `tbl_user_host` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user_mem`
--

DROP TABLE IF EXISTS `tbl_user_mem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_user_mem` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` longtext NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL DEFAULT '1',
  `updated_by` int(11) DEFAULT NULL,
  `secondpassword` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `hostid` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `hostid_idx` (`hostid`),
  CONSTRAINT `hostid` FOREIGN KEY (`hostid`) REFERENCES `tbl_user_host` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user_mem`
--

LOCK TABLES `tbl_user_mem` WRITE;
/*!40000 ALTER TABLE `tbl_user_mem` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_user_mem` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-20 17:58:52
