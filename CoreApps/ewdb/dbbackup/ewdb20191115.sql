-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: ewalletapp
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_menu`
--

DROP TABLE IF EXISTS `tbl_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_menu` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `category` int(11) NOT NULL,
  `description` varchar(999) DEFAULT NULL,
  `volume` double DEFAULT NULL,
  `price` double NOT NULL,
  `discount` double DEFAULT NULL,
  `totalprice` double NOT NULL,
  `unit` int(11) NOT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'S',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL DEFAULT '1',
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `id_idx` (`category`),
  KEY `unitid_idx` (`unit`),
  CONSTRAINT `categoryid` FOREIGN KEY (`category`) REFERENCES `tbl_menu_category` (`id`) ON UPDATE RESTRICT,
  CONSTRAINT `unitId` FOREIGN KEY (`unit`) REFERENCES `tbl_units` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_menu`
--

LOCK TABLES `tbl_menu` WRITE;
/*!40000 ALTER TABLE `tbl_menu` DISABLE KEYS */;
INSERT INTO `tbl_menu` VALUES (1,'Beer',1,'Let\'s get tipsy!',320,7.9,0,7.9,4,'A','2019-11-14 11:51:38','2019-11-14 12:05:47',1,NULL),(2,'Mahjong',3,'Ji Mo 13-yiu!',0,200,0,200,2,'A','2019-11-14 11:54:10','2019-11-14 12:05:47',1,NULL);
/*!40000 ALTER TABLE `tbl_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_menu_category`
--

DROP TABLE IF EXISTS `tbl_menu_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_menu_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'S',
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_type_id_idx` (`type`),
  CONSTRAINT `categorytypeId` FOREIGN KEY (`type`) REFERENCES `tbl_menu_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_menu_category`
--

LOCK TABLES `tbl_menu_category` WRITE;
/*!40000 ALTER TABLE `tbl_menu_category` DISABLE KEYS */;
INSERT INTO `tbl_menu_category` VALUES (1,'Alcohol','A',2),(2,'Main course','A',2),(3,'Table games','A',3),(4,'Card Games','A',3);
/*!40000 ALTER TABLE `tbl_menu_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_menu_type`
--

DROP TABLE IF EXISTS `tbl_menu_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_menu_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_type` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'S',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_menu_type`
--

LOCK TABLES `tbl_menu_type` WRITE;
/*!40000 ALTER TABLE `tbl_menu_type` DISABLE KEYS */;
INSERT INTO `tbl_menu_type` VALUES (1,'None','A'),(2,'Edibles','A'),(3,'Games','A');
/*!40000 ALTER TABLE `tbl_menu_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_units`
--

DROP TABLE IF EXISTS `tbl_units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_units`
--

LOCK TABLES `tbl_units` WRITE;
/*!40000 ALTER TABLE `tbl_units` DISABLE KEYS */;
INSERT INTO `tbl_units` VALUES (1,'night'),(2,'game'),(3,'bottle'),(4,'can'),(5,'pack');
/*!40000 ALTER TABLE `tbl_units` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user_host`
--

DROP TABLE IF EXISTS `tbl_user_host`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_user_host` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` longtext NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL DEFAULT '1',
  `updated_by` int(11) DEFAULT NULL,
  `secondpassword` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user_host`
--

LOCK TABLES `tbl_user_host` WRITE;
/*!40000 ALTER TABLE `tbl_user_host` DISABLE KEYS */;
INSERT INTO `tbl_user_host` VALUES (1,'rootadmin','k+m6AL9cBIqTqj+i/rgT3Rrxh9zA1nU4MLQdomRnZ3k=','2019-11-15 14:57:14','2019-11-15 14:57:14',1,NULL,'123456','0164567895','rootadmin@email.com');
/*!40000 ALTER TABLE `tbl_user_host` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-15 17:55:00
