﻿using ew.front.react.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ew.front.react.Services
{
    public interface IIdentityService
    {
        Task<AuthenticationResult> RegisterHostAsync(IdentityVM.Registration reg);
        Task<AuthenticationResult> LoginHostAsync(IdentityVM.Login login);
        Task<AuthenticationResult> RegisterUserAsync(IdentityVM.Registration reg);
        Task<AuthenticationResult> LoginUserAsync(IdentityVM.Login login);
        Task<AuthenticationResult> RefreshTokenAsync(IdentityVM.RefreshTokenRequest refresh);
    }
}
