﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ew.front.react.Models;
using ewdb.dblib;
using ew.front.react.Helper;
using ew.front.react.Options;
using Microsoft.EntityFrameworkCore;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Security.Claims;
using System.Collections.Generic;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Http;

namespace ew.front.react.Services
{
    public class IdentityService : IIdentityService
    {
        public string Saltkey
        {
            get
            {
                return _saltKeyConfig.SaltKey;
            }
        }

        private readonly mydbctx _ctx;
        private readonly SaltKeySettings _saltKeyConfig;
        private readonly JwtSettings _jwtSettings;
        private readonly TokenValidationParameters _tokenValidationParameters;
        public IdentityService(mydbctx ctx, SaltKeySettings saltkeyconfig, JwtSettings jwtSettings, TokenValidationParameters tokenValidationParameters)
        {
            _ctx = ctx;
            _saltKeyConfig = saltkeyconfig;
            _jwtSettings = jwtSettings;
            _tokenValidationParameters = tokenValidationParameters;
        }

        public async Task<AuthenticationResult> RegisterHostAsync(IdentityVM.Registration reg)
        {
            var existuser = await _ctx.TblUserHost.Where(x => x.Username == reg.Username).SingleOrDefaultAsync();

            if (existuser != null)
            {
                return new AuthenticationResult
                {
                    Errors = new[] { "User with this email address already exists" }
                };
            }

            var createuser = new TblUserHost
            {
                Username = reg.Username,
                Password = PasswordHashHelper.Hash(reg.Password, Saltkey),
                Secondpassword = reg.SecondPassword,
                Email = reg.Email,
                Phone = reg.PhoneNumber
            };

            await _ctx.TblUserHost.AddAsync(createuser);
            await _ctx.SaveChangesAsync();

            var claims = new ClaimsVM
            {
                Id = createuser.Id,
                Username = createuser.Username,
                Email = createuser.Email,
                Type = "host",
                PhoneNumber = createuser.Phone
            };

            return await GenerateAuthenticationResultForUserAsync(claims);
        }

        public async Task<AuthenticationResult> LoginHostAsync(IdentityVM.Login login)
        {
            var getUserInfo = await _ctx.TblUserHost.Where(x => x.Username == login.Username).SingleOrDefaultAsync();

            if (getUserInfo != null)
            {
                var chkPasswordValid = PasswordHashHelper.ConfirmPassword(login.Password, getUserInfo.Password, Saltkey);

                if (chkPasswordValid)
                {

                    var claims = new ClaimsVM
                    {
                        Id = getUserInfo.Id,
                        Username = getUserInfo.Username,
                        Email = getUserInfo.Email,
                        Type = "host",
                        PhoneNumber = getUserInfo.Phone
                    };

                    return await GenerateAuthenticationResultForUserAsync(claims);
                }
            }
            return new AuthenticationResult
            {
                Errors = new[] { "Username/Password is invalid." }
            };
        }

        public Task<AuthenticationResult> RegisterUserAsync(IdentityVM.Registration reg)
        {
            throw new NotImplementedException();
        }

        public Task<AuthenticationResult> LoginUserAsync(IdentityVM.Login login)
        {
            throw new NotImplementedException();
        }

        private async Task<AuthenticationResult> GenerateAuthenticationResultForUserAsync(ClaimsVM user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtSettings.Secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                 {
                    new Claim(JwtRegisteredClaimNames.Sub, user.Username),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Email, user.Email),
                    new Claim("id", user.Id.ToString()),
                    new Claim("type", user.Type)
                 }),
                Expires = DateTime.Now.Add(_jwtSettings.TokenLifetime),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            var refreshToken = new TblTokenRefresh
            {
                Token = Guid.NewGuid().ToString(),
                Jwtid = token.Id,
                Userid = user.Id,
                Creationdate = DateTime.Now,
                Expirydate = DateTime.Now.AddMonths(12)
            };

            await _ctx.TblTokenRefresh.AddAsync(refreshToken);
            await _ctx.SaveChangesAsync();

            return new AuthenticationResult
            {
                Success = true,
                Token = tokenHandler.WriteToken(token),
                RefreshToken = refreshToken.Token
            };
        }

        public async Task<AuthenticationResult> RefreshTokenAsync(IdentityVM.RefreshTokenRequest refresh)
        {
            var validatedToken = GetPrincipalFromToken(refresh.Token);

            if (validatedToken == null)
            {
                return new AuthenticationResult { Errors = new[] { "Invalid token" } };
            }

            var expiryDateUnix = long.Parse(validatedToken.Claims.Single(x => x.Type == JwtRegisteredClaimNames.Exp).Value);

            //convert to GMT +8 malaysia time, UTC is 1970/1/1 00:00:00
            var expiryDateTime = new DateTime(1970, 1, 1, 8, 0, 0, DateTimeKind.Local)
                .AddSeconds(expiryDateUnix);

            if (expiryDateTime > DateTime.Now)
            {
                return new AuthenticationResult { Errors = new[] { "This token hasn't expired yet" } };
            }

            var jti = validatedToken.Claims.Single(x => x.Type == JwtRegisteredClaimNames.Jti).Value;

            var storedRefreshToken = await _ctx.TblTokenRefresh.SingleOrDefaultAsync(x => x.Token == refresh.RefreshToken);

            if (storedRefreshToken == null)
            {
                return new AuthenticationResult { Errors = new[] { "This refresh token doesn't exist." } };
            }

            if (DateTime.Now > storedRefreshToken.Expirydate)
            {
                return new AuthenticationResult { Errors = new[] { "This refresh token has expired." } };
            }

            if (storedRefreshToken.Invalidated == 1)
            {
                return new AuthenticationResult { Errors = new[] { "This refresh token has been invalidated." } };
            }

            if (storedRefreshToken.Used == 1)
            {
                return new AuthenticationResult { Errors = new[] { "This refresh token has been used." } };
            }

            if (storedRefreshToken.Jwtid != jti)
            {
                return new AuthenticationResult { Errors = new[] { "This refresh token doesn't match JWT." } };
            }

            storedRefreshToken.Used = 1;
            _ctx.TblTokenRefresh.Update(storedRefreshToken);
            await _ctx.SaveChangesAsync();

            var userIdFromClaim = Int32.Parse(validatedToken.Claims.Single(x => x.Type == "id").Value);
            var userTypeFromClaim = validatedToken.Claims.Single(x => x.Type == "type").Value;

            var claims = new ClaimsVM { };

            if (userTypeFromClaim == "host")
            {
                claims = await _ctx.TblUserHost.Where(x => x.Id == userIdFromClaim).Select(x => new ClaimsVM
                {
                    Id = x.Id,
                    Email = x.Email,
                    Username = x.Username,
                    Type = userTypeFromClaim,
                    PhoneNumber = x.Phone
                }).SingleOrDefaultAsync();
            }
            else if (userTypeFromClaim == "mem")
            {
                claims = await _ctx.TblUserMem.Where(x => x.Id == userIdFromClaim).Select(x => new ClaimsVM
                {
                    Id = x.Id,
                    Email = x.Email,
                    Username = x.Username,
                    Type = userTypeFromClaim,
                    PhoneNumber = x.Phone
                }).SingleOrDefaultAsync();
            }
            else
            {
                return new AuthenticationResult { Errors = new[] { "User type is invalid." } };
            }

            return await GenerateAuthenticationResultForUserAsync(claims);
        }

        //Check and validate OLD token helper, return null if token is invalid/tampered
        private ClaimsPrincipal GetPrincipalFromToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            try
            {
                var principal = tokenHandler.ValidateToken(token, _tokenValidationParameters, out var validatedToken);
                if (!IsJWTWithValidSecurityAlgh(validatedToken))
                {
                    return null;
                }
                return principal;
            }
            catch
            {
                return null;
            }
        }

        private bool IsJWTWithValidSecurityAlgh(SecurityToken validatedToken)
        {
            return (validatedToken is JwtSecurityToken jwtSecurityToken) &&
                jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256,
                StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
