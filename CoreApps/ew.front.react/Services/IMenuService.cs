﻿using ew.front.react.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using ew.front.react.Contracts.v1.Response;
using System.Threading.Tasks;

namespace ew.front.react.Services
{
    public interface IMenuService
    {
        Task<int> CreateMenuAsync(MenuVM.Menu food);
        Task<bool> UpdateMenuAsync(int menuid, MenuVM.Menu foodUpdate);
        Task<List<MenuResponse.Menu>> GetAllMenuAsync();
        Task<MenuResponse.Menu> GetMenuByIdAsync(int menuId);

        Task<int> CreateCategoryAsync(MenuVM.Category cat);
        Task<bool> UpdateCategoryAsync(int catid, MenuVM.Category catUpdate);
        Task<List<MenuResponse.Category>> GetAllCategoryAsync();
        Task<MenuResponse.Category> GetCategoryByIdAsync(int catid);

        Task<bool> CheckCategoryExistByNameAsync(string catname);
        Task<bool> CheckCategoryExistByIdAsync(int catid);
        Task<bool> CheckCategoryMatchByIdAsync(int catid, string m_catname);
        Task<bool> CheckCatTypeExistByIdAsync(int catid);
        Task<bool> CheckUnitExistByIdAsync(int unitid);
    }
}
