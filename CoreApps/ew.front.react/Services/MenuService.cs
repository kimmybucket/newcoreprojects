﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ew.front.react.Models;
using ewdb.dblib;
using Microsoft.EntityFrameworkCore;
using ew.front.react.Contracts.v1.Response;

namespace ew.front.react.Services
{
    public class MenuService : IMenuService
    {
        private readonly mydbctx _ctx; //MOD : our dbcontext injected here
        public MenuService(mydbctx ctx)
        {
            _ctx = ctx;
        }

        public async Task<int> CreateMenuAsync(MenuVM.Menu food)
        {
            var data = new TblMenu
            {
                Name = food.Name,
                Category = food.Category,
                Price = food.Price,
                Discount = food.Discount,
                Totalprice = food.TotalPrice,
                Unit = food.Unit,
                Description = food.Description,
                Volume = food.Volume,
                Status = (food.Status.ToUpper() == "A") ? "A" : "S"
            };

            await _ctx.TblMenu.AddAsync(data);
            await _ctx.SaveChangesAsync();

            int newid = data.Id;

            return newid;
        }

        public async Task<List<MenuResponse.Menu>> GetAllMenuAsync()
        {
            var data = await _ctx.TblMenu.Select(x => new MenuResponse.Menu
            {
                Id = x.Id,
                Name = x.Name,
                Category = x.Category,
                CategoryName = x.CategoryNavigation.Name ?? "No category",
                CategoryTypeName = x.CategoryNavigation.TypeNavigation.CategoryType,
                Description = x.Description,
                Price = x.Price,
                Discount = x.Discount.Value,
                Unit = x.UnitNavigation.UnitName,
                TotalPrice = x.Totalprice,
                Status = x.Status,
                Volume = x.Volume.Value
            }).ToListAsync();

            return data;
        }

        public async Task<MenuResponse.Menu> GetMenuByIdAsync(int menuId)
        {
            var data = await _ctx.TblMenu.Where(x => x.Id == menuId).Select(x => new MenuResponse.Menu
            {
                Name = x.Name,
                Category = x.Category,
                CategoryName = x.CategoryNavigation.Name ?? "No category",
                CategoryTypeName = x.CategoryNavigation.TypeNavigation.CategoryType,
                Description = x.Description,
                Price = x.Price,
                Discount = x.Discount.Value,
                TotalPrice = x.Totalprice,
                Unit = x.UnitNavigation.UnitName,
                Status = x.Status,
                Volume = x.Volume.Value
            }).SingleOrDefaultAsync();

            return data;
        }

        public async Task<bool> UpdateMenuAsync(int menu_id, MenuVM.Menu foodUpdate)
        {
            var menubyid = await _ctx.TblMenu.Where(x => x.Id == menu_id).SingleOrDefaultAsync();

            if (menubyid != null)
            {
                menubyid.Name = foodUpdate.Name;
                menubyid.Category = foodUpdate.Category;
                menubyid.Description = foodUpdate.Description;
                menubyid.Price = foodUpdate.Price;
                menubyid.Discount = foodUpdate.Discount;
                menubyid.Totalprice = foodUpdate.TotalPrice;
                menubyid.Unit = foodUpdate.Unit;
                menubyid.Status = (foodUpdate.Status.ToUpper() == "A") ? "A" : "S";
                menubyid.Volume = foodUpdate.Volume;
                var updated = await _ctx.SaveChangesAsync();
                return updated > 0;
            }
            return false;
        }

        public async Task<int> CreateCategoryAsync(MenuVM.Category cat)
        {
            var data = new TblMenuCategory
            {
                Name = cat.CategoryName,
                Status = (cat.Status.ToUpper() == "A") ? "A" : "S",
                Type = cat.CategoryTypeId
            };
            await _ctx.TblMenuCategory.AddAsync(data);
            await _ctx.SaveChangesAsync();

            var newid = data.Id;

            return newid;
        }
        public async Task<List<MenuResponse.Category>> GetAllCategoryAsync()
        {
            var data = await _ctx.TblMenuCategory.Select(x => new MenuResponse.Category
            {
                Id = x.Id,
                CategoryName = x.Name,
                CategoryTypeId = x.Type,
                CategoryTypeName = x.TypeNavigation.CategoryType,
                Status = x.Status
            }).ToListAsync();

            return data;
        }
        public async Task<bool> UpdateCategoryAsync(int catid, MenuVM.Category catUpdate)
        {
            var catByID = await _ctx.TblMenuCategory.Where(x => x.Id == catid).SingleOrDefaultAsync();

            if (catByID != null)
            {
                catByID.Name = catUpdate.CategoryName;
                catByID.Status = (catUpdate.Status.ToUpper() == "A") ? "A" : "S";
                catByID.Type = catUpdate.CategoryTypeId;

                var updated = await _ctx.SaveChangesAsync();
                return updated > 0;
            }

            return false;
        }

        public async Task<MenuResponse.Category> GetCategoryByIdAsync(int catid)
        {
            var data = await _ctx.TblMenuCategory.Where(x => x.Id == catid).Select(x => new MenuResponse.Category
            {
                Id = x.Id,
                CategoryName = x.Name,
                CategoryTypeId = x.Type,
                CategoryTypeName = x.TypeNavigation.CategoryType,
                Status = x.Status
            }).SingleOrDefaultAsync();

            return data;
        }

        public async Task<bool> CheckCategoryExistByNameAsync(string catname)
        {
            var chkCategory = await _ctx.TblMenuCategory.Where(o => o.Name == catname).SingleOrDefaultAsync();
            if (chkCategory == null)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> CheckCategoryExistByIdAsync(int catid)
        {
            var chkCategoryId = await _ctx.TblMenuCategory.Where(o => o.Id == catid).SingleOrDefaultAsync();
            if (chkCategoryId == null)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> CheckCategoryMatchByIdAsync(int catid, string m_catname)
        {
            var getname = await _ctx.TblMenuCategory.Where(x => x.Id == catid).Select(x => x.Name).SingleOrDefaultAsync();

            if (getname != null)
            {
                return (getname == m_catname) ? true : false;
            }

            return false;
        }

        public async Task<bool> CheckCatTypeExistByIdAsync(int typeid)
        {
            var chkCategoryTypeId = await _ctx.TblMenuType.Where(o => o.Id == typeid).SingleOrDefaultAsync();
            if (chkCategoryTypeId == null)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> CheckUnitExistByIdAsync(int unitid)
        {
            var chkUnitTypeId = await _ctx.TblUnits.Where(o => o.Id == unitid).SingleOrDefaultAsync();
            if (chkUnitTypeId == null)
            {
                return false;
            }

            return true;
        }
    }
}
