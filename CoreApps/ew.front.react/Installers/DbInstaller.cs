﻿using ew.front.react.Services;
using ewdb.dblib;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ew.front.react.Installers
{
    public class DbInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<mydbctx>(options => options.UseMySql(configuration.GetConnectionString("ewdbase")));
            services.AddScoped<IMenuService, MenuService>();
            services.AddScoped<IIdentityService, IdentityService>();
        }
    }
}
