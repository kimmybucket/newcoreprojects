import React, { Component } from 'react';
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { DropdownToggle, DropdownMenu, DropdownItem, UncontrolledDropdown } from 'reactstrap';
import { Link } from 'react-router-dom';
import './NavMenu.css';

export class NavMenu extends Component {
    static displayName = NavMenu.name;

    static beforeAuthNav(tgl) {
        return (
            <div>
                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret>
                        I'm a Host
        </DropdownToggle>
                    <DropdownMenu right>
                        <DropdownItem>
                            <NavLink onClick={tgl.toggleNavbar} tag={Link} className="text-dark" to="/host/registration">Registration</NavLink>
                        </DropdownItem>

                        <DropdownItem>
                            <NavLink onClick={tgl.toggleNavbar} tag={Link} className="text-dark" to="/host/login">Login</NavLink>
                        </DropdownItem>

                    </DropdownMenu>
                </UncontrolledDropdown>

                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret>
                        I'm a Member
            </DropdownToggle>
                    <DropdownMenu right>
                        <DropdownItem>
                            <NavLink onClick={tgl.toggleNavbar} tag={Link} className="text-dark" to="/registration">Registration</NavLink>
                        </DropdownItem>

                        <DropdownItem>
                            <NavLink onClick={tgl.toggleNavbar} tag={Link} className="text-dark" to="/login">Login</NavLink>
                        </DropdownItem>
                    </DropdownMenu>
                </UncontrolledDropdown>
            </div >
        )
    }

    static afterAuthNav(tgl) {
        return (
            <div>
                <NavItem>
                    <NavLink onClick={tgl.toggleNavbar} tag={Link} className="text-dark" to="/menu">Menus</NavLink>
                </NavItem>

                <NavItem>
                    <NavLink tag={Link} className="text-dark" to="/host/logout">Logout</NavLink>
                </NavItem>
            </div>
        )
    }


    constructor(props) {
        super(props);

        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            collapsed: true,
            isOpen: false,
            isAuth: true
        };

    }

    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }

    toggleNavbar() {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }

    render() {
        return (
            <header>
                <Navbar className="navbar-expand-sm navbar-toggleable-sm ng-white border-bottom box-shadow mb-3" light>
                    <Container>
                        <NavbarBrand tag={Link} to="/">XOUL</NavbarBrand>
                        <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
                        <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={!this.state.collapsed} navbar>
                            <ul className="navbar-nav flex-grow">

                                <NavItem>
                                    <NavLink tag={Link} className="text-dark" to="/">Home</NavLink>
                                </NavItem>

                                {
                                    this.state.isAuth
                                        ? this.state.isAuth && NavMenu.afterAuthNav(this)
                                        : NavMenu.beforeAuthNav(this)
                                }

                            </ul>
                        </Collapse>
                    </Container>
                </Navbar>
            </header>
        );
    }
}
