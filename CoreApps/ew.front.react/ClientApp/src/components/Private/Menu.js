﻿import React, { Component } from 'react';
import { Card } from 'reactstrap';
import MenuService from '../../services/MenuService';

export class PrivateMenu extends Component {
    static displayName = PrivateMenu.name;

    constructor(props) {
        super(props);
        this.state = { menuarray: [], loading: true }
        this.getMenus()
    }

    getMenus() {
        MenuService.getAllMenu().then(data => {

            if (data !== undefined) {
                this.setState({ menuarray: data, loading: false });
            } else {
                this.setState({ loading: false });
            }

        })
    }

    static renderMenu(menus) {
        return (
            menus.map(item =>
                <Card key={item.id}>
                    {item.name}
                </Card>
            )
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : PrivateMenu.renderMenu(this.state.menuarray);

        return (
            <div>
                {contents}
            </div>
        )
    }
}