﻿import React, { Component } from 'react';
import { Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import AccountService from '../../services/AccountService';

export class HostLogin extends Component {
    static displayName = HostLogin.name;

    constructor(props) {
        super(props);

        this.change = this.change.bind(this);
        this.login = this.login.bind(this);

        this.state = {
            username: '',
            password: ''
        };

    }

    change(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    async login(e) {
        e.preventDefault();

        await AccountService.doHostLogin(this.state.username, this.state.password).then(isauth => {
            if (isauth) {
                this.props.history.push('/menu')
            }
        })
    }

    render() {
        return (
            <Form>
                <FormGroup row>
                    <Label for="username" sm={2}>Host Username</Label>
                    <Col sm={10}>
                        <Input type="text" name="username" placeholder="Username" onChange={e => this.change(e)} />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="password" sm={2}>Password</Label>
                    <Col sm={10}>
                        <Input type="password" name="password" placeholder="Password" onChange={e => this.change(e)} />
                    </Col>
                </FormGroup>
                <Button className="w-100" onClick={this.login}>LOGIN</Button>
            </Form>
        )
    }
}