﻿import React, { Component } from 'react';
import { Redirect } from "react-router-dom"
import AccountService from '../../services/AccountService';

export class HostLogout extends Component {
    constructor(props) {
        super(props);
        AccountService.doHostLogout()
    }
    render() {
        return (
            <Redirect to="/host/login" />
        )
    }
}