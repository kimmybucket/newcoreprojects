﻿import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import JWTService from '../services/JWTService';

export const ProtectedRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
        JWTService.isAuth() === true
            ? <Component {...props} />
            : <Redirect to={{pathname: '/host/login', state: {from: props.location}}} />
    )} />
)