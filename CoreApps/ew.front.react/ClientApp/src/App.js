import React, { Component } from 'react';
import { Route, Switch } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';
import { Counter } from './components/Counter';
import { HostLogin } from './components/Host/Login';
import { PrivateMenu } from './components/Private/Menu';
import { HostLogout } from './components/Host/logout';
import { ProtectedRoute } from './components/PrivateRoute';

export default class App extends Component {
    static displayName = App.name;

    render() {
        return (
            <Layout>
                <Switch>
                    <Route exact path='/' component={Home} />
                    <Route path='/counter' component={Counter} />
                    <Route path='/fetch-data' component={FetchData} />
                    <Route path='/host/login' component={HostLogin} />
                    <ProtectedRoute path='/menu' component={PrivateMenu} />
                    <Route path='/host/logout' component={HostLogout} />
                    <Route path='*' component={() => "404 NOT FOUND"} />
                </Switch>
            </Layout>
        );
    }
}
