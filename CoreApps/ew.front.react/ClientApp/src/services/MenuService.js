﻿import JWTService from './JWTService';

class MenuService {
    
    async getAllMenu() {
        var jwts = JWTService.getJWT()

        let response = await fetch('api/v1/menus', {
            method: 'get',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + jwts.token
            }
        })

        console.log(response)

        let responseStatus = await response.status;
        
        if (responseStatus === 200) {
            //console.log(responseJson)
            let responseJson = await response.json();
            return responseJson.response;
        } else {
            console.log("Internal error.")
        }
    }
}

export default new MenuService();