﻿class AccountService {

    async doHostLogout() {
        localStorage.removeItem('_jwt');
    }

    async doHostLogin(usernm, pwd) {
        let response = await fetch('api/v1/host/login', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: usernm,
                password: pwd
            })
        })

        let responseStatus = await response.status;
        let responseJson = await response.json();

        if (responseStatus === 200) {
            var tokenRes = {
                "token": responseJson.token,
                "refreshToken": responseJson.refreshToken
            }

            localStorage.setItem('_jwt', JSON.stringify(tokenRes))
            console.log("Login success")
            return true
            // Retrieve the object from storage
            //var val = JSON.parse(localStorage.getItem('_jwt'));
        } else {
            console.log(responseStatus);
            localStorage.removeItem('_jwt');

            let responseErr = responseJson.errors;

            if (responseErr !== undefined) {
                //check first item of array has 'key', for focusing input's id when validating if gt error
                //key returns which model's property name
                if (responseErr[0].hasOwnProperty('key')) {
                    //with key, highlight input box
                    console.log("has 'key'")
                } else {
                    //no key, just show toast alert
                    console.log("no 'key'")
                }
                console.log(responseErr);
            } else {
                console.log("Internal error")
            }
        }
    }
}

export default new AccountService();