﻿class JWTService {
    getJWT() {
        var jwt = JSON.parse(window.localStorage.getItem('_jwt'))

        this.props = {
            token: "",
            refreshtoken: ""
        }

        if (jwt !== null) {
            this.props = {
                token: jwt.token,
                refreshtoken: jwt.refreshToken
            }
        }

        return this.props
    }

    isAuth() {
        return true;
    }
}

export default new JWTService()