﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ew.front.react.Models
{
    public class MenuVM
    {
        public class Menu
        {
            [Required]
            public string Name { get; set; }
            [Required]
            public int Category { get; set; }
            public string Description { get; set; }
            [Range(0, double.MaxValue, ErrorMessage = "Please enter valid volume.")]
            public double? Volume { get; set; }
            [Required]
            [RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Please enter valid price.")]
            public double Price { get; set; }
            [RegularExpression(@"^(0*100{1,1}\.?((?<=\.)0*)?%?$)|(^0*\d{0,2}\.?((?<=\.)\d*)?%?)$", ErrorMessage = "Please enter valid discount percentage.")]
            public double? Discount { get; set; }
            public double TotalPrice
            {
                get { return Math.Round(this.Price * (1 - (this.Discount.Value / 100)), 2); }
            }
            [Required]
            public int Unit;
            public string Status;
        }

        public class Category
        {
            [Required]
            public string CategoryName { get; set; }
            [Required]
            public int CategoryTypeId { get; set; }
            public string Status { get; set; }
        }
    }
}
