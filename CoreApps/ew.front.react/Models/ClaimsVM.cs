﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ew.front.react.Models
{
    public class ClaimsVM
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public int Id { get; set; }
        public string Type { get; set; }
        public string PhoneNumber { get; set; }
    }
}
