﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ew.front.react.Models
{
    public class IdentityVM
    {
        public class Registration
        {
            [Required]
            [MinLength(8, ErrorMessage = "Username must be at least 8 characters.")]
            public string Username { get; set; }
            [Required]
            [MinLength(8, ErrorMessage = "Password must be at least 8 characters.")]
            public string Password { get; set; }
            [Required]
            [RegularExpression(@"^[0-9]{6}$", ErrorMessage = "Secondary Password must be 6 digit numbers.")]
            public string SecondPassword { get; set; }
            [Required]
            [RegularExpression(@"^(\+?6?01)[0-46-9]-*[0-9]{7,8}$", ErrorMessage = "Please enter a VALID phone number.")]
            public string PhoneNumber { get; set; }
            //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Please enter a VALID email address.")]
            [EmailAddress(ErrorMessage = "Please enter a VALID email address.")]
            public string Email { get; set; }
        }

        public class Login
        {
            [Required]
            [MinLength(8, ErrorMessage = "Username must be at least 8 characters.")]
            public string Username { get; set; }
            [Required]
            [MinLength(8, ErrorMessage = "Password must be at least 8 characters.")]
            public string Password { get; set; }
        }

        public class RefreshTokenRequest
        {
            [Required]
            public string Token { get; set; }

            [Required]
            public string RefreshToken { get; set; }
        }
    }
}
