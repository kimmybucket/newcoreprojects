﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ew.front.react.Contracts.v1.Response
{
    public class MenuResponse
    {
        public class Menu
        {
            public int Id { get; set; }
            public string CategoryTypeName { get; set; }
            public string CategoryName { get; set; }
            public string Name { get; set; }
            public int Category { get; set; }
            public string Description { get; set; }
            public double Volume { get; set; }
            public double Price { get; set; }
            public double Discount { get; set; }
            public double TotalPrice { get; set; }
            public string Unit { get; set; }
            public string Status { get; set; }
        }

        public class Category
        {
            public int Id { get; set; }
            public string CategoryName { get; set; }
            public int CategoryTypeId { get; set; }
            public string CategoryTypeName { get; set; }
            public string Status { get; set; }
        }
    }
}
