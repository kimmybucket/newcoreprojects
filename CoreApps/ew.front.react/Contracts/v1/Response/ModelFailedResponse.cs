﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ew.front.react.Contracts.v1.Response
{
    public class ModelFailedResponse
    {
        public IEnumerable<ErrorAttr> Errors { get; set; }

        public class ErrorAttr {
            public string key { get; set; }
            public string errMsg { get; set; }
        }
    }
}
