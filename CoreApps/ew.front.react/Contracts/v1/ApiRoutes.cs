﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ew.front.react.Contracts.v1
{
    public class ApiRoutes
    {
        public const string Root = "api";
        public const string Version = "v1";
        public const string Base = Root + "/" + Version;

        public static class Auths
        {
            public const string RefreshToken = Base + "/refreshToken";

            public const string LoginHost = Base + "/host/login";
            public const string RegisterHost = Base + "/host/registration";

            public const string LoginUser = Base + "/login";
            public const string RegisterUser = Base + "/registration";
        }

        public static class Menus
        {
            public const string Create = Base + "/menus";
            public const string GetAll = Base + "/menus";
            public const string GetById = Base + "/menus/{menu_id}";
            public const string Update = Base + "/menus/{menu_id}";

            public static class Category
            {
                public const string Create = Base + "/menus/category";
                public const string Update = Base + "/menus/category/{cat_id}";
                public const string GetAll = Base + "/menus/category";
                public const string GetById = Base + "/menus/category/{cat_id}";
            }
        }
    }
}
