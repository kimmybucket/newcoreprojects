﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using ew.front.react.Models;
using ewdb.dblib;

namespace ew.front.react.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)] //hide API from public
    [Route("api/v1/[controller]")]
    public class AuthController : Controller
    {
        private readonly mydbctx _ctx; //MOD : our dbcontext injected here
        public AuthController(mydbctx ctx)
        {
            this._ctx = ctx;
        }

        [HttpGet("login")]
        public IActionResult Login(LoginVM model) 
        {
            var password = _ctx.TblUserHost.Where(o => o.Username == "ADMIN").Select(o => o.Password).SingleOrDefault().ToString();
            return Ok(new { token = password });
        }

    }
}