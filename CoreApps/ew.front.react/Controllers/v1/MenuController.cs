﻿using ew.front.react.Contracts.v1;
using ew.front.react.Contracts.v1.Response;
using ew.front.react.Helper;
using ew.front.react.Models;
using ew.front.react.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace ew.front.react.Controllers.v1
{
    [Authorize(Policy = "HostOnly")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class MenuController : ControllerBase
    {
        private readonly IMenuService _menuService;
        public MenuController(IMenuService menuService)
        {
            _menuService = menuService;
        }

        [HttpPost(ApiRoutes.Menus.Create)]
        public async Task<IActionResult> Create([FromBody] MenuVM.Menu model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelFailedResponse
                {
                    Errors = ModelValidationHelper.ValidateModelState(ModelState)
                });
            }
            else
            {
                var chkCategoryId = await _menuService.CheckCategoryExistByIdAsync(model.Category);
                var chkUnitId = await _menuService.CheckUnitExistByIdAsync(model.Unit);

                if (chkCategoryId)
                {
                    if (chkUnitId)
                    {
                        int newid = await _menuService.CreateMenuAsync(model);
                        return Ok(new { id = newid });
                    }
                    return BadRequest(new { Error = "Unit type doesn't exist." });
                }
                return BadRequest(new { Error = "Category doesn't exist." });
            }
        }

        [HttpPut(ApiRoutes.Menus.Update)]
        public async Task<IActionResult> Update([FromRoute]int menu_id, [FromBody] MenuVM.Menu model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelFailedResponse
                {
                    Errors = ModelValidationHelper.ValidateModelState(ModelState)
                });
            }
            else
            {
                var chkCategoryId = await _menuService.CheckCategoryExistByIdAsync(model.Category);
                var chkUnitId = await _menuService.CheckUnitExistByIdAsync(model.Unit);

                if (chkCategoryId)
                {
                    if (chkUnitId)
                    {
                        var updateMenu = await _menuService.UpdateMenuAsync(menu_id, model);
                        if (updateMenu)
                        {
                            var menuById = await _menuService.GetMenuByIdAsync(menu_id);
                            return Ok(new { id = menu_id, updated = menuById });
                        }
                        return NotFound();
                    }
                    return BadRequest(new { Error = "Unit type doesn't exist." });
                }
                return BadRequest(new { Error = "Category doesn't exist." });
            }
        }

        [HttpGet(ApiRoutes.Menus.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            var menus = await _menuService.GetAllMenuAsync();
            if (menus == null)
            {
                return NotFound();
            }

            return Ok(new { response = menus });
        }

        [HttpGet(ApiRoutes.Menus.GetById)]
        public async Task<IActionResult> GetById([FromRoute] int menu_id)
        {
            var menu = await _menuService.GetMenuByIdAsync(menu_id);

            if (menu == null)
            {
                return NotFound();
            }

            return Ok(new { response = menu });
        }

        //------------------------------------ Category ------------------------------

        [HttpPost(ApiRoutes.Menus.Category.Create)]
        public async Task<IActionResult> Create([FromBody] MenuVM.Category model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelFailedResponse
                {
                    Errors = ModelValidationHelper.ValidateModelState(ModelState)
                });
            }
            else
            {
                var chkCategory = await _menuService.CheckCategoryExistByNameAsync(model.CategoryName);
                var chkCatType = await _menuService.CheckCatTypeExistByIdAsync(model.CategoryTypeId);

                if (chkCategory)
                {
                    if (chkCatType)
                    {
                        int newid = await _menuService.CreateCategoryAsync(model);
                        return Ok(new { id = newid });
                    }
                    return BadRequest(new { Error = "Type doesn't exist." });
                }
                return BadRequest(new { Error = "Category already exists." });
            }
        }

        [HttpPut(ApiRoutes.Menus.Category.Update)]
        public async Task<IActionResult> Update([FromRoute]int cat_id, [FromBody] MenuVM.Category model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelFailedResponse
                {
                    Errors = ModelValidationHelper.ValidateModelState(ModelState)
                });
            }
            else
            {
                var chkCategory = await _menuService.CheckCategoryExistByNameAsync(model.CategoryName);
                var chkCatNameMatch = await _menuService.CheckCategoryMatchByIdAsync(cat_id, model.CategoryName);
                var chkCatType = await _menuService.CheckCatTypeExistByIdAsync(model.CategoryTypeId);

                //check if update model's name same with query's name, same then allow update
                // else check whether name exists
                if (chkCategory || chkCatNameMatch)
                {
                    if (chkCatType)
                    {
                        var updateCat = await _menuService.UpdateCategoryAsync(cat_id, model);
                        if (updateCat)
                        {
                            var catById = await _menuService.GetCategoryByIdAsync(cat_id);
                            return Ok(new { updated = catById });
                        }
                        return NotFound();
                    }
                    return BadRequest(new { Error = "Type doesn't exist." });
                }
                return BadRequest(new { Error = "Category already exists." });
            }
        }

        [HttpGet(ApiRoutes.Menus.Category.GetAll)]
        public async Task<IActionResult> GetAllCategory()
        {
            var cats = await _menuService.GetAllCategoryAsync();
            if (cats == null)
            {
                return NotFound();
            }

            return Ok(new { response = cats });
        }

        [HttpGet(ApiRoutes.Menus.Category.GetById)]
        public async Task<IActionResult> GetCategoryById([FromRoute] int cat_id)
        {
            var cat = await _menuService.GetCategoryByIdAsync(cat_id);

            if (cat == null)
            {
                return NotFound();
            }

            return Ok(new { response = cat });
        }
    }
}