﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ew.front.react.Contracts.v1;
using ew.front.react.Contracts.v1.Response;
using ew.front.react.Helper;
using ew.front.react.Models;
using ew.front.react.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ew.front.react.Controllers.v1
{
    public class IdentityController : ControllerBase
    {
        private readonly IIdentityService _identityService;
        public IdentityController(IIdentityService identityService)
        {
            _identityService = identityService;
        }

        [HttpPost(ApiRoutes.Auths.RegisterHost)]
        public async Task<IActionResult> RegistrationHost([FromBody] IdentityVM.Registration model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelFailedResponse
                {
                    Errors = ModelValidationHelper.ValidateModelState(ModelState)
                });
            }
            else
            {
                var authResponse = await _identityService.RegisterHostAsync(model);
                if (!authResponse.Success)
                {
                    return BadRequest(new AuthFailedResponse
                    {
                        Errors = authResponse.Errors
                    });
                }

                return Ok(new AuthSuccessResponse
                {
                    Token = authResponse.Token,
                    RefreshToken = authResponse.RefreshToken
                });
            }
        }

        [HttpPost(ApiRoutes.Auths.LoginHost)]
        public async Task<IActionResult> LoginHost([FromBody] IdentityVM.Login model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelFailedResponse
                {
                    Errors = ModelValidationHelper.ValidateModelState(ModelState)
                });
            }
            else
            {
                var authResponse = await _identityService.LoginHostAsync(model);
                if (!authResponse.Success)
                {
                    return BadRequest(new AuthFailedResponse
                    {
                        Errors = authResponse.Errors
                    });
                }

                return Ok(new AuthSuccessResponse
                {
                    Token = authResponse.Token,
                    RefreshToken = authResponse.RefreshToken
                });
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost(ApiRoutes.Auths.RefreshToken)]
        public async Task<IActionResult> RefreshToken([FromBody] IdentityVM.RefreshTokenRequest model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelFailedResponse
                {
                    Errors = ModelValidationHelper.ValidateModelState(ModelState)
                });
            }
            else
            {
                var authResponse = await _identityService.RefreshTokenAsync(model);
                if (!authResponse.Success)
                {
                    return BadRequest(new AuthFailedResponse
                    {
                        Errors = authResponse.Errors
                    });
                }

                return Ok(new AuthSuccessResponse
                {
                    Token = authResponse.Token,
                    RefreshToken = authResponse.RefreshToken
                });
            }
        }
    }
}