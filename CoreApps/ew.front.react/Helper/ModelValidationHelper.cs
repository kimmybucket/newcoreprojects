﻿using ew.front.react.Contracts.v1.Response;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ew.front.react.Helper
{
    public static class ModelValidationHelper
    {
        public static List<ModelFailedResponse.ErrorAttr> ValidateModelState(ModelStateDictionary modelstate) {
            //var allErrors = ModelState.Values.SelectMany(v => v.Errors.Select(b => b.ErrorMessage)); first version
            var allErrors = new List<ModelFailedResponse.ErrorAttr>();
            foreach (var modelError in modelstate)
            {
                string modelKey = modelError.Key;
                if (modelError.Value.Errors.Count > 0)
                {
                    allErrors.Add(new ModelFailedResponse.ErrorAttr
                    {
                        key = modelKey,
                        errMsg = modelError.Value.Errors.Select(b => b.ErrorMessage).FirstOrDefault()
                    });
                }
            }

            return allErrors;
        }
    }
}
