﻿using ew.front.react.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ew.front.react.Helper
{
    public class PasswordHashHelper
    {
        //https://stackoverflow.com/questions/2138429/hash-and-salt-passwords-in-c-sharp
        //Hash to byte[] converter
        public static string Hash(string value, string saltkey)
        {
            return Convert.ToBase64String(Hash(Encoding.UTF8.GetBytes(value), Encoding.UTF8.GetBytes(saltkey)));
        }

        //Hashing function
        private static byte[] Hash(byte[] value, byte[] salt)
        {
            byte[] saltedValue = value.Concat(salt).ToArray();
            return new SHA256Managed().ComputeHash(saltedValue);
        }

        //Password matching
        public static bool ConfirmPassword(string password, string sysPassword, string saltKey)
        {
            string passwordHash = Hash(password, saltKey);

            return sysPassword.SequenceEqual(passwordHash);
        }
    }
}
