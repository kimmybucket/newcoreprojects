﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ewallet.Models;

namespace ewallet.Controllers
{
    public class AccountController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(AccountVM.UserLogin model)
        {
            if (ModelState.IsValid)
            {
                ModelState.AddModelError("success", "Submit success");
            }
            else
            {
                ModelState.AddModelError("error", "Submit failed");
            }
            return View("Index", model);
        }
    }
}