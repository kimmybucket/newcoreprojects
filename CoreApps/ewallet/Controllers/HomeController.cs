﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ewallet.Models;
using ewdb.dblib;

namespace ewallet.Controllers
{
    public class HomeController : Controller
    {
        private mydbctx _ctx; //MOD : our dbcontext injected here
        public HomeController(mydbctx _ctx) //MOD : inject to this controller
        {
            this._ctx = _ctx;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
