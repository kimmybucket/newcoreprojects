﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ewallet.Models
{
    public class AccountVM
    {
        public class UserLogin
        {
            [Required]
            [StringLength(100,MinimumLength =8, ErrorMessage ="Min. 8 characters")]
            public string Username { get; set; }
            [Required]
            [StringLength(100, MinimumLength = 8, ErrorMessage = "Min. 8 characters")]
            public string Password { get; set; }
        }
    }
}
